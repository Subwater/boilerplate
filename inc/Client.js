//Author Jimmy Spearman <jimmysper@gmail.com>

const EventEmitter = require('events').EventEmitter;
const MySQL = require('mysql'); // Change to mysql2

class Client extends EventEmitter {
	constructor(cId, cSocket, cSql, cServer) {
		//Init EventEmitter
		super();

		this.id = cId;
		this.permissions = {};
		this.db = null;
		this._server = cServer;
		this._socket = cSocket;

		//EventEmitter-Socket.io interlop
		this._socket.use(p => {
			this.emit(p[0], p[1]);
			console.log('Emitting ' + p[0]);
		});

		//Init MySQL connection
		this.db = MySQL.createConnection(cSql);
		this.db.connect();
	}

	//Authenticate user
	login (user, pass) {
		axios.post('/user', {
			username: 'Fred',
			password: 'Flintstone'
		}).then(res => {
			console.log(response);
		}).catch(err => {
			console.log(err);
		});
	}

	destroy () {
		this._db.end();
		this._socket.close();
		//TODO: handle more destroy
	}

	//Get a Client from a Collection where key: value === Client's property and value
	static getClient (cArray, key, value) {
		for (let client in cArray)
			if (client.hasOwnProperty(Object.keys(key)[0]) &&
				client[Object.keys(key)[0]] === Object.values(value)[0])
				return client;
	};
}

//Add EventEmitter functionality to the Client
Client.prototype = Object.create(require('events').EventEmitter.prototype);

//Export
module.exports = Client;


