const express = require('express');
const router = express.Router();

const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const es6Renderer = require('express-es6-template-engine');
const logger = require('morgan');
const session = require('express-session');
const FileStore = require('session-file-store')(session);

// Attach middleware to app
[logger('dev'),
express.json(),
express.urlencoded({ extended: false }),
cookieParser(),
session({
    store: new FileStore({
        reapInterval: 1800
    }),
    secret: config.keys.cookie,
    saveUninitialized: false,
    resave: false,
    cookie: {
        maxAge: 60000
    }
})].forEach(middleware => router.use(middleware));

// View engine setup
router.engine('html', es6Renderer); // Set renderer and extention
router.set('views', './views'); // Specify the views directory
router.set('view engine', 'html'); // Register the template engine

// GET route home 
router.get('/', (req, res, next) => {
    res.render('index', { locals: { title: 'Express' } });
});

// Catch 404 and forward to error handler
router.use((req, res, next) => {
    next(createError(404));
});

// Error handler
router.use((err, req, res, next) => {
    // Set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // Render the error page
    res.status(err.status || 500);
    res.render('error');
});


module.exports = router;