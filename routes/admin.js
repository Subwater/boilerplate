const express = require('express');
const router = express.Router();

const app = require('../app');

/* GET home page. */
router.get('/', (req, res, next) => {
    req.app.locals.tlog('DEBUG', 'Working!');
    res.render('index', { locals: { title: 'Express' } });
});

module.exports = router;
