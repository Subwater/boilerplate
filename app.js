// Import modules
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const express = require('express');
const cookieParser = require('cookie-parser');
const color = require('colors');
const createError = require('http-errors');
const es6Renderer = require('express-es6-template-engine');
const logger = require('morgan');
const session = require('express-session');
const FileStore = require('session-file-store')(session);

let app = express();

// Load config
const config = JSON.parse(fs.readFileSync('./config.json'));

// Setup app specific props
app.name = 'node-boilerplate';
app.locals.author = 'Jimmy Spearman';
app.locals.email = 'jimmysper@gmail.com';
app.locals.debug = true; // TODO: Change this when needed
app.locals.tlog = tlog;
app.locals.df = require('dateformat');
app.locals.df.masks.tns = 'mm/dd/yy hh:MM:ssT';

// Clear console
process.stdout.write('\033c');

// Setup express
app.enable('trust proxy');

// Attach middleware to app
[logger('dev'),
express.json(),
express.urlencoded({ extended: false }),
cookieParser(),
session({
    store: new FileStore({
        reapInterval: 1800
    }),
    secret: config.keys.cookie,
    saveUninitialized: false,
    resave: false,
    cookie: {
        maxAge: 60000
    }
})].forEach(middleware => app.use(middleware));

// View engine setup
app.engine('html', es6Renderer); // Set renderer and extention
app.set('views', './views'); // Specify the views directory
app.set('view engine', 'html'); // Register the template engine

// Attach routes to app
_.forIn({
    '/': require('./routes/index'),
    'admin': require('./routes/admin'),
    'user': require('./routes/users')
}, (v, k) => {
    app.locals.tlog('INFO', `Added ${k.cyan} to app`);
    app.use((k.includes('/') ? k : '/' + k), v);
});


// Catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// Error handler
app.use((err, req, res, next) => {
    // Set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // Render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;

// FUNCTIONS 
function tlog(level, message) {

    let stamp = app.locals.df(new Date(), 'tns');

    level = level === undefined ? 'default' : level.toUpperCase();
    message = message === undefined ? 'default' : message;

    if (level.includes('ERROR')) {
        level = level.red
        message = message.red
    }
    if (level.includes('WARN')) {
        level = level.yellow
        message = message.yellow
    }
    if (level.includes('DEBUG')) {
        level = level.cyan
        message = message.cyan

    }

    console.log('[' + stamp + '] [' + (level !== 'default' && message == 'default' ? message : level + '] ' + message));
}